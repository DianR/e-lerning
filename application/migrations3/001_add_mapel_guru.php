<?php

class Migration_Add_Mapel_Guru extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'id_mapel' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                        ),
                        
                        'id_guru' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('mapel_guru');
        }

        public function down()
        {
                $this->dbforge->drop_table('mapel_guru');
        }
}
