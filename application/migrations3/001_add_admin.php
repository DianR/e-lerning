<?php

class Migration_Add_Admin extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'nip' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '15',
                        ),
                        'nama_adm' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                        'email' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                        'user_id' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('admin');
        }

        public function down()
        {
                $this->dbforge->drop_table('admin');
        }
}
