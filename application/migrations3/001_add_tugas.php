<?php

class Migration_Add_Tugas extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'id_mapel' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                        ),
                        'sesi' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '25',
                        ),
                        'waktu_mulai' => array(
                                'type' => 'DATETIME',
                                
                        ),
                        'waktu_selesai' => array(
                                'type' => 'DATETIME',
                        ),
                        'id_guru' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('tugas');
        }

        public function down()
        {
                $this->dbforge->drop_table('tugas');
        }
}
