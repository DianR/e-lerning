<?php

class Migration_Add_Mapel_Siswa extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'id_mapel' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                        ),
                        
                        'id_siswa' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('mapel_siswa');
        }

        public function down()
        {
                $this->dbforge->drop_table('mapel_siswa');
        }
}
