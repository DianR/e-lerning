<?php

class Migration_Add_Mapel extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'kode_mapel' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '25',
                        ),
                        
                        'nama_mapel' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('mapel');
        }

        public function down()
        {
                $this->dbforge->drop_table('mapel');
        }
}
