<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Tugas List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Mapel</th>
		<th>Sesi</th>
		<th>Waktu Mulai</th>
		<th>Waktu Selesai</th>
		<th>Id Guru</th>
		
            </tr><?php
            foreach ($tugas_data as $tugas)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $tugas->id_mapel ?></td>
		      <td><?php echo $tugas->sesi ?></td>
		      <td><?php echo $tugas->waktu_mulai ?></td>
		      <td><?php echo $tugas->waktu_selesai ?></td>
		      <td><?php echo $tugas->id_guru ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>