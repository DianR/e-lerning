<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>E-LEARNING </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Ultra Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url() ?>web/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url() ?>web/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<link rel="icon" href="favicon.ico" type="image/x-icon" >
<!-- font-awesome icons -->
<link href="<?php echo base_url() ?>web/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
 <!-- js-->
<script src="<?php echo base_url() ?>web/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url() ?>web/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Comfortaa:400,700,300' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Muli:400,300,300italic,400italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->  
<!-- Metis Menu -->
<script src="<?php echo base_url() ?>web/js/metisMenu.min.js"></script>
<script src="<?php echo base_url() ?>web/js/custom.js"></script>
<link href="<?php echo base_url() ?>web/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<!--Calender-->
<link rel="stylesheet" href="<?php echo base_url() ?>web/css/clndr.css" type="text/css" />
<script src="<?php echo base_url() ?>web/js/underscore-min.js" type="text/javascript"></script>
<script src= "<?php echo base_url() ?>web/js/moment-2.2.1.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>web/js/clndr.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>web/js/site.js" type="text/javascript"></script>
<!--End Calender-->
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
                <!--left-fixed -navigation-->
		
		<!--left-fixed -navigation-->
		<!-- header-starts -->
		<div class="sticky-header header-section ">
			<div class="header-left">
				<!--logo -->
				<div class="logo">
					<a href="dashboard"><h1>E-LEARNING</h1></a>
				</div>
				<!--//logo-->
				
			</div>
			<div class="pull-right">
                                    <ul class="list-inline" type="none">
										<li class="list-inline-item"> <a href="<?=base_url() ?>dashboard">Home</a></li>
                                        <li class="list-inline-item"> My Course</li>
                                        <li class="list-inline-item"> List Course</li>
										<li class="list-inline-item"> About</li>
										<li class="list-inline-item"> Help</li>
                                        <li class="list-inline-item">
                                            <a href="<?=base_url() ?>auth/logout">Logout</a>
                                            
                                        </li>
                                    </ul>
				<!--toggle button end-->
			</div>
		</div>
		<div class="main-content">
		<!--left-fixed -navigation-->
		<div class="sidebar" role="navigation">
            <div class="navbar-collapse">
				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right dev-page-sidebar mCustomScrollbar _mCS_1 mCS-autoHide mCS_no_scrollbar" id="cbp-spmenu-s1">
					<div class="scrollbar scrollbar1">
						<ul class="nav" id="side-menu">
							<li>
								<a href="#"><i class="fa fa-cogs nav_icon"></i>Components <span class="fa arrow"></span></a>
								<ul class="nav nav-second-level collapse">
									<li>
										<a href="<?=base_url() ?>siswa">Data Siswa</a>
									</li>
									<li>
										<a href="<?=base_url() ?>guru">Data Guru</a>
									</li>
									<li>
										<a href="<?=base_url() ?>materi">Materi</a>
									</li>
									<li>
										<a href="<?=base_url() ?>tugas">Tugas</a>
									</li>
									<li>
										<a href="<?=base_url() ?>mapel_siswa">Mapel Siswa</a>
									</li>
									<li>
										<a href="<?=base_url() ?>mapel_guru">Mapel Guru</a>
									</li>
								</ul>
								<!-- /nav-second-level -->
							</li>
						</ul>
					</div>
					<!-- //sidebar-collapse -->
				</nav>
			</div>
		</div>
                <style>
                    .pull-right {
                        margin-right: 20px;
                    }
                </style>
		<!-- //header-ends -->
		<!-- //header-ends -->