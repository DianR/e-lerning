
        <div id="page-wrapper">
            <div class="main-page">

           
                   

                <div class="grid-section">
                        <h2 class="hdg">Materi</h2>
                    <div class="row mb40">
                                <div class="col-md-12 table-grid">
                                <div class="panel panel-widget">
                                    <div class="bs-docs-example">
                                        
<!-- isi content -->
<div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                 <a href="<?=base_url() ?>materi/create" title="Tambah data"><img src="<?=base_url() ?>add.png" width="75" ></a>
               <!-- <?php echo anchor(site_url('materi/create'),'Create', 'class="btn btn-primary"'); ?> -->
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('materi/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('materi'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>

        <table class="table table-striped" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Mapel</th>
		<th>Sesi</th>
		<th>Id Guru</th>
		<th>File</th>
		<th>Action</th>
            </tr><?php
            foreach ($materi_data as $materi)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $materi->id_mapel ?></td>
			<td><?php echo $materi->sesi ?></td>
			<td><?php echo $materi->id_guru ?></td>
			<td><?php echo $materi->file ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('materi/read/'.$materi->id),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-default btn-sm')); 
				echo '  '; 
				echo anchor(site_url('materi/update/'.$materi->id),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')); 
				echo '  '; 
				echo anchor(site_url('materi/delete/'.$materi->id),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
		<?php echo anchor(site_url('materi/excel'), 'Excel', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('materi/word'), 'Word', 'class="btn btn-primary"'); ?>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>

        


                                    </div>
                                </div>
                            </div>

                </div>
                </div>
                </div>

                </div>
                <div class="clearfix"> </div>
            </div>
            </div>
            </div>



   