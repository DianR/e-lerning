<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>E-LEARNING LPGO</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Ultra Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url() ?>web/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url() ?>web/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<link rel="icon" href="favicon.ico" type="image/x-icon" >
<!-- font-awesome icons -->
<link href="<?php echo base_url() ?>web/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
 <!-- js-->
<script src="<?php echo base_url() ?>web/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url() ?>web/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Comfortaa:400,700,300' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Muli:400,300,300italic,400italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->  
<!-- Metis Menu -->
<script src="<?php echo base_url() ?>web/js/metisMenu.min.js"></script>
<script src="<?php echo base_url() ?>web/js/custom.js"></script>
<link href="<?php echo base_url() ?>web/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
</head> 
<body class="cbp-spmenu-push">
    <div class="main-content">
        
        <div id="page-wrapper">
            <div class="main-page">
                <div class="login-form">
                    <h4>Login</h4>
                    <h5><strong>Welcome</strong> login to get started!</h5>
                    <form method="post" action="">
                        <input type="text" placeholder="Username" name="username" required>
                        <input type="password" class="pass" placeholder="Password" name="password" required>
                        <span class="check-left"><input type="checkbox">Remember me</span>
                        <span class="check-right"><a href="#">Forgot password?</a></span>
                        <div class="clearfix"></div>
                        <button class="btn btn-info btn-block" type="submit">Sign in</button>
                      
                        <p class="center-block mg-t mg-b">Dont have and account?
                        <a href="signup.html">Signup here.</a>
                        </p>
                    </form>
                </div>
        </div>  
    </div>  
    <!--typo-ends-->
    
<?php $this->load->view('footer'); ?>