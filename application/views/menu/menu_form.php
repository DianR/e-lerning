
<div id="page-wrapper">
            <div class="main-page">
                <div class="validation-section">
                    <h2></h2>
                    <div class="col-md-6 validation-grid">
                        <h4><span>Menu</span> </h4>
                        <div class="validation-grid1">
                            <div class="valid-top">
                                <form id="defaultForm" method="post" class="form-horizontal" action="<?php echo $action; ?>">
                                    <div class="form-group">

	    <div class="form-group">
            <label class="col-lg-3 control-label" for="varchar">Name <?php echo form_error('name') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="varchar">Link <?php echo form_error('link') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="link" id="link" placeholder="Link" value="<?php echo $link; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="varchar">Icon <?php echo form_error('icon') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="icon" id="icon" placeholder="Icon" value="<?php echo $icon; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="int">Is Active <?php echo form_error('is_active') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="is_active" id="is_active" placeholder="Is Active" value="<?php echo $is_active; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="int">Is Parent <?php echo form_error('is_parent') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="is_parent" id="is_parent" placeholder="Is Parent" value="<?php echo $is_parent; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="varchar">User List <?php echo form_error('user_list') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="user_list" id="user_list" placeholder="User List" value="<?php echo $user_list; ?>" /></div>
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    
                                    <div class="form-group">
                                        <div class="col-lg-9 col-lg-offset-3">
                                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                                       

 
	    
                                    
                                            <a href="<?php echo site_url('menu') ?>" class="btn btn-default">Cancel</a>
                                        </div>
                                    </div>

	
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>

   