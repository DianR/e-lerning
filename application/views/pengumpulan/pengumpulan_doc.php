<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Pengumpulan List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Tugas</th>
		<th>Id Siswa</th>
		<th>Tgl Upload</th>
		<th>File</th>
		
            </tr><?php
            foreach ($pengumpulan_data as $pengumpulan)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $pengumpulan->id_tugas ?></td>
		      <td><?php echo $pengumpulan->id_siswa ?></td>
		      <td><?php echo $pengumpulan->tgl_upload ?></td>
		      <td><?php echo $pengumpulan->file ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>