
        <div id="page-wrapper">
            <div class="main-page">

           
                   

                <div class="grid-section">
                        <h2 class="hdg">Migrations</h2>
                    <div class="row mb40">
                                <div class="col-md-12 table-grid">
                                <div class="panel panel-widget">
                                    <div class="bs-docs-example">
                                        
<!-- isi content -->
<div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                 <a href="<?=base_url() ?>migrations/create" title="Tambah data"><img src="<?=base_url() ?>add.png" width="75" ></a>
               <!-- <?php echo anchor(site_url('migrations/create'),'Create', 'class="btn btn-primary"'); ?> -->
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('migrations/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('migrations'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>

        <table class="table table-striped" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Version</th>
		<th>Action</th>
            </tr><?php
            foreach ($migrations_data as $migrations)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $migrations->version ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('migrations/read/'.$migrations->),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-default btn-sm')); 
				echo '  '; 
				echo anchor(site_url('migrations/update/'.$migrations->),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')); 
				echo '  '; 
				echo anchor(site_url('migrations/delete/'.$migrations->),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
		<?php echo anchor(site_url('migrations/excel'), 'Excel', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('migrations/word'), 'Word', 'class="btn btn-primary"'); ?>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>

        


                                    </div>
                                </div>
                            </div>

                </div>
                </div>
                </div>

                </div>
                <div class="clearfix"> </div>
            </div>
            </div>
            </div>



   