
<div id="page-wrapper">
            <div class="main-page">
                <div class="validation-section">
                    <h2></h2>
                    <div class="col-md-6 validation-grid">
                        <h4><span>Menu_role</span> </h4>
                        <div class="validation-grid1">
                            <div class="valid-top">
                                <form id="defaultForm" method="post" class="form-horizontal" action="<?php echo $action; ?>">
                                    <div class="form-group">

	    <div class="form-group">
            <label class="col-lg-3 control-label" for="varchar">Title <?php echo form_error('title') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?php echo $title; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="varchar">Link Url <?php echo form_error('link_url') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="link_url" id="link_url" placeholder="Link Url" value="<?php echo $link_url; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="enum">Role <?php echo form_error('role') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="role" id="role" placeholder="Role" value="<?php echo $role; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="enum">Parent <?php echo form_error('parent') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="parent" id="parent" placeholder="Parent" value="<?php echo $parent; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="int">Parent Id <?php echo form_error('parent_id') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="parent_id" id="parent_id" placeholder="Parent Id" value="<?php echo $parent_id; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="varchar">Icon <?php echo form_error('icon') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="icon" id="icon" placeholder="Icon" value="<?php echo $icon; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="enum">Stat <?php echo form_error('stat') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="stat" id="stat" placeholder="Stat" value="<?php echo $stat; ?>" /></div>
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    
                                    <div class="form-group">
                                        <div class="col-lg-9 col-lg-offset-3">
                                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                                       

 
	    
                                    
                                            <a href="<?php echo site_url('menu_role') ?>" class="btn btn-default">Cancel</a>
                                        </div>
                                    </div>

	
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>

   