<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Menu_role List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Title</th>
		<th>Link Url</th>
		<th>Role</th>
		<th>Parent</th>
		<th>Parent Id</th>
		<th>Icon</th>
		<th>Stat</th>
		
            </tr><?php
            foreach ($menu_role_data as $menu_role)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $menu_role->title ?></td>
		      <td><?php echo $menu_role->link_url ?></td>
		      <td><?php echo $menu_role->role ?></td>
		      <td><?php echo $menu_role->parent ?></td>
		      <td><?php echo $menu_role->parent_id ?></td>
		      <td><?php echo $menu_role->icon ?></td>
		      <td><?php echo $menu_role->stat ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>