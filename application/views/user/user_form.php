
<div id="page-wrapper">
            <div class="main-page">
                <div class="validation-section">
                    <h2></h2>
                    <div class="col-md-6 validation-grid">
                        <h4><span>User</span> </h4>
                        <div class="validation-grid1">
                            <div class="valid-top">
                                <form id="defaultForm" method="post" class="form-horizontal" action="<?php echo $action; ?>">
                                    <div class="form-group">

	    <div class="form-group">
            <label class="col-lg-3 control-label" for="varchar">Username <?php echo form_error('username') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="varchar">Password <?php echo form_error('password') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="enum">Role <?php echo form_error('role') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="role" id="role" placeholder="Role" value="<?php echo $role; ?>" /></div>
        </div>
	    <div class="form-group">
            <label class="col-lg-3 control-label" for="varchar">Email <?php echo form_error('email') ?></label>
           <div class="col-lg-5"> <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" /></div>
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    
                                    <div class="form-group">
                                        <div class="col-lg-9 col-lg-offset-3">
                                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                                       

 
	    
                                    
                                            <a href="<?php echo site_url('user') ?>" class="btn btn-default">Cancel</a>
                                        </div>
                                    </div>

	
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>

   