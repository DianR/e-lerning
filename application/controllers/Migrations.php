<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migrations extends MY_Controller {
    
    protected $access = array('Admin');

    function __construct()
    {
        parent::__construct();
        $this->load->model('Migrations_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'migrations/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'migrations/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'migrations/index.html';
            $config['first_url'] = base_url() . 'migrations/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Migrations_model->total_rows($q);
        $migrations = $this->Migrations_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'migrations_data' => $migrations,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        $this->load->view('header');
        $this->load->view('migrations/migrations_list', $data);
        $this->load->view('footer');
    }

    public function read($id) 
    {
        $row = $this->Migrations_model->get_by_id($id);
        if ($row) {
            $data = array(
		'version' => $row->version,
	    );
            $this->load->view('header');
            $this->load->view('migrations/migrations_read', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('migrations'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('migrations/create_action'),
	    'version' => set_value('version'),
	);

        $this->load->view('header');
        $this->load->view('migrations/migrations_form', $data);
        $this->load->view('footer');
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'version' => $this->input->post('version',TRUE),
	    );

            $this->Migrations_model->insert($data);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-success" id="alert">Create Record Success</div></div>');
            redirect(site_url('migrations'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Migrations_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('migrations/update_action'),
		'version' => set_value('version', $row->version),
	    );

            $this->load->view('header');
            $this->load->view('migrations/migrations_form', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('migrations'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('', TRUE));
        } else {
            $data = array(
		'version' => $this->input->post('version',TRUE),
	    );

            $this->Migrations_model->update($this->input->post('', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-info" id="alert">Update Record Success</div></div>');
            redirect(site_url('migrations'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Migrations_model->get_by_id($id);

        if ($row) {
            $this->Migrations_model->delete($id);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-danger" id="alert">Delete Record Success</div></div>');
            redirect(site_url('migrations'));
        } else {
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-info" id="alert">Record Not Found</div></div>');
            redirect(site_url('migrations'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('version', 'version', 'trim|required');

	$this->form_validation->set_rules('', '', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "migrations.xls";
        $judul = "migrations";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Version");

	foreach ($this->Migrations_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->version);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=migrations.doc");

        $data = array(
            'migrations_data' => $this->Migrations_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('migrations/migrations_doc',$data);
    }

}

/* End of file Migrations.php */
/* Location: ./application/controllers/Migrations.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-06-05 16:32:54 */
/* http://harviacode.com */