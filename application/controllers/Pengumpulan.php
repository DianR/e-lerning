<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengumpulan extends MY_Controller {
    
    protected $access = array('Admin');

    function __construct()
    {
        parent::__construct();
        $this->load->model('Pengumpulan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pengumpulan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pengumpulan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pengumpulan/index.html';
            $config['first_url'] = base_url() . 'pengumpulan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pengumpulan_model->total_rows($q);
        $pengumpulan = $this->Pengumpulan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pengumpulan_data' => $pengumpulan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        $this->load->view('header');
        $this->load->view('pengumpulan/pengumpulan_list', $data);
        $this->load->view('footer');
    }

    public function read($id) 
    {
        $row = $this->Pengumpulan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'id_tugas' => $row->id_tugas,
		'id_siswa' => $row->id_siswa,
		'tgl_upload' => $row->tgl_upload,
		'file' => $row->file,
	    );
            $this->load->view('header');
            $this->load->view('pengumpulan/pengumpulan_read', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengumpulan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('pengumpulan/create_action'),
	    'id' => set_value('id'),
	    'id_tugas' => set_value('id_tugas'),
	    'id_siswa' => set_value('id_siswa'),
	    'tgl_upload' => set_value('tgl_upload'),
	    'file' => set_value('file'),
	);

        $this->load->view('header');
        $this->load->view('pengumpulan/pengumpulan_form', $data);
        $this->load->view('footer');
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_tugas' => $this->input->post('id_tugas',TRUE),
		'id_siswa' => $this->input->post('id_siswa',TRUE),
		'tgl_upload' => $this->input->post('tgl_upload',TRUE),
		'file' => $this->input->post('file',TRUE),
	    );

            $this->Pengumpulan_model->insert($data);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-success" id="alert">Create Record Success</div></div>');
            redirect(site_url('pengumpulan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Pengumpulan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('pengumpulan/update_action'),
		'id' => set_value('id', $row->id),
		'id_tugas' => set_value('id_tugas', $row->id_tugas),
		'id_siswa' => set_value('id_siswa', $row->id_siswa),
		'tgl_upload' => set_value('tgl_upload', $row->tgl_upload),
		'file' => set_value('file', $row->file),
	    );

            $this->load->view('header');
            $this->load->view('pengumpulan/pengumpulan_form', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengumpulan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'id_tugas' => $this->input->post('id_tugas',TRUE),
		'id_siswa' => $this->input->post('id_siswa',TRUE),
		'tgl_upload' => $this->input->post('tgl_upload',TRUE),
		'file' => $this->input->post('file',TRUE),
	    );

            $this->Pengumpulan_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-info" id="alert">Update Record Success</div></div>');
            redirect(site_url('pengumpulan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pengumpulan_model->get_by_id($id);

        if ($row) {
            $this->Pengumpulan_model->delete($id);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-danger" id="alert">Delete Record Success</div></div>');
            redirect(site_url('pengumpulan'));
        } else {
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-info" id="alert">Record Not Found</div></div>');
            redirect(site_url('pengumpulan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_tugas', 'id tugas', 'trim|required');
	$this->form_validation->set_rules('id_siswa', 'id siswa', 'trim|required');
	$this->form_validation->set_rules('tgl_upload', 'tgl upload', 'trim|required');
	$this->form_validation->set_rules('file', 'file', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "pengumpulan.xls";
        $judul = "pengumpulan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Tugas");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Siswa");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Upload");
	xlsWriteLabel($tablehead, $kolomhead++, "File");

	foreach ($this->Pengumpulan_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_tugas);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_siswa);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_upload);
	    xlsWriteNumber($tablebody, $kolombody++, $data->file);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=pengumpulan.doc");

        $data = array(
            'pengumpulan_data' => $this->Pengumpulan_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('pengumpulan/pengumpulan_doc',$data);
    }

}

/* End of file Pengumpulan.php */
/* Location: ./application/controllers/Pengumpulan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-06-05 16:32:54 */
/* http://harviacode.com */