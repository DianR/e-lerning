<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu_role extends MY_Controller {
    
    protected $access = array('Admin');

    function __construct()
    {
        parent::__construct();
        $this->load->model('Menu_role_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'menu_role/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'menu_role/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'menu_role/index.html';
            $config['first_url'] = base_url() . 'menu_role/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Menu_role_model->total_rows($q);
        $menu_role = $this->Menu_role_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'menu_role_data' => $menu_role,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        $this->load->view('header');
        $this->load->view('menu_role/menu_role_list', $data);
        $this->load->view('footer');
    }

    public function read($id) 
    {
        $row = $this->Menu_role_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'title' => $row->title,
		'link_url' => $row->link_url,
		'role' => $row->role,
		'parent' => $row->parent,
		'parent_id' => $row->parent_id,
		'icon' => $row->icon,
		'stat' => $row->stat,
	    );
            $this->load->view('header');
            $this->load->view('menu_role/menu_role_read', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('menu_role'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('menu_role/create_action'),
	    'id' => set_value('id'),
	    'title' => set_value('title'),
	    'link_url' => set_value('link_url'),
	    'role' => set_value('role'),
	    'parent' => set_value('parent'),
	    'parent_id' => set_value('parent_id'),
	    'icon' => set_value('icon'),
	    'stat' => set_value('stat'),
	);

        $this->load->view('header');
        $this->load->view('menu_role/menu_role_form', $data);
        $this->load->view('footer');
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'title' => $this->input->post('title',TRUE),
		'link_url' => $this->input->post('link_url',TRUE),
		'role' => $this->input->post('role',TRUE),
		'parent' => $this->input->post('parent',TRUE),
		'parent_id' => $this->input->post('parent_id',TRUE),
		'icon' => $this->input->post('icon',TRUE),
		'stat' => $this->input->post('stat',TRUE),
	    );

            $this->Menu_role_model->insert($data);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-success" id="alert">Create Record Success</div></div>');
            redirect(site_url('menu_role'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Menu_role_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('menu_role/update_action'),
		'id' => set_value('id', $row->id),
		'title' => set_value('title', $row->title),
		'link_url' => set_value('link_url', $row->link_url),
		'role' => set_value('role', $row->role),
		'parent' => set_value('parent', $row->parent),
		'parent_id' => set_value('parent_id', $row->parent_id),
		'icon' => set_value('icon', $row->icon),
		'stat' => set_value('stat', $row->stat),
	    );

            $this->load->view('header');
            $this->load->view('menu_role/menu_role_form', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('menu_role'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'title' => $this->input->post('title',TRUE),
		'link_url' => $this->input->post('link_url',TRUE),
		'role' => $this->input->post('role',TRUE),
		'parent' => $this->input->post('parent',TRUE),
		'parent_id' => $this->input->post('parent_id',TRUE),
		'icon' => $this->input->post('icon',TRUE),
		'stat' => $this->input->post('stat',TRUE),
	    );

            $this->Menu_role_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-info" id="alert">Update Record Success</div></div>');
            redirect(site_url('menu_role'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Menu_role_model->get_by_id($id);

        if ($row) {
            $this->Menu_role_model->delete($id);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-danger" id="alert">Delete Record Success</div></div>');
            redirect(site_url('menu_role'));
        } else {
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-info" id="alert">Record Not Found</div></div>');
            redirect(site_url('menu_role'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('title', 'title', 'trim|required');
	$this->form_validation->set_rules('link_url', 'link url', 'trim|required');
	$this->form_validation->set_rules('role', 'role', 'trim|required');
	$this->form_validation->set_rules('parent', 'parent', 'trim|required');
	$this->form_validation->set_rules('parent_id', 'parent id', 'trim|required');
	$this->form_validation->set_rules('icon', 'icon', 'trim|required');
	$this->form_validation->set_rules('stat', 'stat', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "menu_role.xls";
        $judul = "menu_role";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Title");
	xlsWriteLabel($tablehead, $kolomhead++, "Link Url");
	xlsWriteLabel($tablehead, $kolomhead++, "Role");
	xlsWriteLabel($tablehead, $kolomhead++, "Parent");
	xlsWriteLabel($tablehead, $kolomhead++, "Parent Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Icon");
	xlsWriteLabel($tablehead, $kolomhead++, "Stat");

	foreach ($this->Menu_role_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->title);
	    xlsWriteLabel($tablebody, $kolombody++, $data->link_url);
	    xlsWriteLabel($tablebody, $kolombody++, $data->role);
	    xlsWriteLabel($tablebody, $kolombody++, $data->parent);
	    xlsWriteNumber($tablebody, $kolombody++, $data->parent_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->icon);
	    xlsWriteLabel($tablebody, $kolombody++, $data->stat);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=menu_role.doc");

        $data = array(
            'menu_role_data' => $this->Menu_role_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('menu_role/menu_role_doc',$data);
    }

}

/* End of file Menu_role.php */
/* Location: ./application/controllers/Menu_role.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-05-22 16:04:15 */
/* http://harviacode.com */