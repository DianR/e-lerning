<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Materi extends MY_Controller {
    
    protected $access = array('Admin');

    function __construct()
    {
        parent::__construct();
        $this->load->model('Materi_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'materi/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'materi/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'materi/index.html';
            $config['first_url'] = base_url() . 'materi/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Materi_model->total_rows($q);
        $materi = $this->Materi_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'materi_data' => $materi,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        $this->load->view('header');
        $this->load->view('materi/materi_list', $data);
        $this->load->view('footer');
    }

    public function read($id) 
    {
        $row = $this->Materi_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'id_mapel' => $row->id_mapel,
		'sesi' => $row->sesi,
		'id_guru' => $row->id_guru,
		'file' => $row->file,
	    );
            $this->load->view('header');
            $this->load->view('materi/materi_read', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('materi'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('materi/create_action'),
	    'id' => set_value('id'),
	    'id_mapel' => set_value('id_mapel'),
	    'sesi' => set_value('sesi'),
	    'id_guru' => set_value('id_guru'),
	    'file' => set_value('file'),
	);

        $this->load->view('header');
        $this->load->view('materi/materi_form', $data);
        $this->load->view('footer');
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_mapel' => $this->input->post('id_mapel',TRUE),
		'sesi' => $this->input->post('sesi',TRUE),
		'id_guru' => $this->input->post('id_guru',TRUE),
		'file' => $this->input->post('file',TRUE),
	    );

            $this->Materi_model->insert($data);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-success" id="alert">Create Record Success</div></div>');
            redirect(site_url('materi'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Materi_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('materi/update_action'),
		'id' => set_value('id', $row->id),
		'id_mapel' => set_value('id_mapel', $row->id_mapel),
		'sesi' => set_value('sesi', $row->sesi),
		'id_guru' => set_value('id_guru', $row->id_guru),
		'file' => set_value('file', $row->file),
	    );

            $this->load->view('header');
            $this->load->view('materi/materi_form', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('materi'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'id_mapel' => $this->input->post('id_mapel',TRUE),
		'sesi' => $this->input->post('sesi',TRUE),
		'id_guru' => $this->input->post('id_guru',TRUE),
		'file' => $this->input->post('file',TRUE),
	    );

            $this->Materi_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-info" id="alert">Update Record Success</div></div>');
            redirect(site_url('materi'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Materi_model->get_by_id($id);

        if ($row) {
            $this->Materi_model->delete($id);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-danger" id="alert">Delete Record Success</div></div>');
            redirect(site_url('materi'));
        } else {
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-info" id="alert">Record Not Found</div></div>');
            redirect(site_url('materi'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_mapel', 'id mapel', 'trim|required');
	$this->form_validation->set_rules('sesi', 'sesi', 'trim|required');
	$this->form_validation->set_rules('id_guru', 'id guru', 'trim|required');
	$this->form_validation->set_rules('file', 'file', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "materi.xls";
        $judul = "materi";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Mapel");
	xlsWriteLabel($tablehead, $kolomhead++, "Sesi");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Guru");
	xlsWriteLabel($tablehead, $kolomhead++, "File");

	foreach ($this->Materi_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_mapel);
	    xlsWriteLabel($tablebody, $kolombody++, $data->sesi);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_guru);
	    xlsWriteNumber($tablebody, $kolombody++, $data->file);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=materi.doc");

        $data = array(
            'materi_data' => $this->Materi_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('materi/materi_doc',$data);
    }

}

/* End of file Materi.php */
/* Location: ./application/controllers/Materi.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-06-05 16:32:54 */
/* http://harviacode.com */