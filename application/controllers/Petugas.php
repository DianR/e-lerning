<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Petugas extends MY_Controller {
    
    protected $access = array('Admin');

    function __construct()
    {
        parent::__construct();
        $this->load->model('Petugas_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'petugas/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'petugas/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'petugas/index.html';
            $config['first_url'] = base_url() . 'petugas/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Petugas_model->total_rows($q);
        $petugas = $this->Petugas_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'petugas_data' => $petugas,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        $this->load->view('header');
        $this->load->view('petugas/petugas_list', $data);
        $this->load->view('footer');
    }

    public function read($id) 
    {
        $row = $this->Petugas_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_petugas' => $row->id_petugas,
		'nama_petugas' => $row->nama_petugas,
		'jabatan' => $row->jabatan,
		'username' => $row->username,
	    );
            $this->load->view('header');
            $this->load->view('petugas/petugas_read', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('petugas'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('petugas/create_action'),
	    'id_petugas' => set_value('id_petugas'),
	    'nama_petugas' => set_value('nama_petugas'),
	    'jabatan' => set_value('jabatan'),
	    'username' => set_value('username'),
	);

        $this->load->view('header');
        $this->load->view('petugas/petugas_form', $data);
        $this->load->view('footer');
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_petugas' => $this->input->post('nama_petugas',TRUE),
		'jabatan' => $this->input->post('jabatan',TRUE),
		'username' => $this->input->post('username',TRUE),
	    );

            $this->Petugas_model->insert($data);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-success" id="alert">Create Record Success</div></div>');
            redirect(site_url('petugas'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Petugas_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('petugas/update_action'),
		'id_petugas' => set_value('id_petugas', $row->id_petugas),
		'nama_petugas' => set_value('nama_petugas', $row->nama_petugas),
		'jabatan' => set_value('jabatan', $row->jabatan),
		'username' => set_value('username', $row->username),
	    );

            $this->load->view('header');
            $this->load->view('petugas/petugas_form', $data);
            $this->load->view('footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('petugas'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_petugas', TRUE));
        } else {
            $data = array(
		'nama_petugas' => $this->input->post('nama_petugas',TRUE),
		'jabatan' => $this->input->post('jabatan',TRUE),
		'username' => $this->input->post('username',TRUE),
	    );

            $this->Petugas_model->update($this->input->post('id_petugas', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-info" id="alert">Update Record Success</div></div>');
            redirect(site_url('petugas'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Petugas_model->get_by_id($id);

        if ($row) {
            $this->Petugas_model->delete($id);
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-danger" id="alert">Delete Record Success</div></div>');
            redirect(site_url('petugas'));
        } else {
            $this->session->set_flashdata('message', '<div class="col-md-12"><div class="alert alert-info" id="alert">Record Not Found</div></div>');
            redirect(site_url('petugas'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_petugas', 'nama petugas', 'trim|required');
	$this->form_validation->set_rules('jabatan', 'jabatan', 'trim|required');
	$this->form_validation->set_rules('username', 'username', 'trim|required');

	$this->form_validation->set_rules('id_petugas', 'id_petugas', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "petugas.xls";
        $judul = "petugas";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Petugas");
	xlsWriteLabel($tablehead, $kolomhead++, "Jabatan");
	xlsWriteLabel($tablehead, $kolomhead++, "Username");

	foreach ($this->Petugas_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_petugas);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jabatan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->username);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=petugas.doc");

        $data = array(
            'petugas_data' => $this->Petugas_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('petugas/petugas_doc',$data);
    }

}

/* End of file Petugas.php */
/* Location: ./application/controllers/Petugas.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-05-22 16:04:16 */
/* http://harviacode.com */