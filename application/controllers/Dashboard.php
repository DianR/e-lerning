<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller can be accessed 
 * for all logged in users
 */
class Dashboard extends MY_Controller {

    protected $access = array('Admin');
    
	public function index()
	{
       // $data['pemesanan'] = $this->db->get('pemesanan');
       // $data['kendaraan'] = $this->db->get('kendaraan');
       //$data['produk'] = $this->db->get('produk');
        $role = $this->session->get_userdata();
        if($role) {
             $data['role'] = $role['role'];
        } else {
            $data['role'] = null;
        }
        $this->load->view('header', $data);
        $this->load->view('index', $data);
        $this->load->view('footer', $data);
        
	}


}