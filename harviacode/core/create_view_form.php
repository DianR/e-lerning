<?php 

$string = "
<div id=\"page-wrapper\">
            <div class=\"main-page\">
                <div class=\"validation-section\">
                    <h2></h2>
                    <div class=\"col-md-6 validation-grid\">
                        <h4><span>".ucfirst($table_name)."</span> </h4>
                        <div class=\"validation-grid1\">
                            <div class=\"valid-top\">
                                <form id=\"defaultForm\" method=\"post\" class=\"form-horizontal\" action=\"<?php echo \$action; ?>\">
                                    <div class=\"form-group\">
";
foreach ($non_pk as $row) {
    if ($row["data_type"] == 'text')
    {
    $string .= "\n\t    <div class=\"form-group\">
            <label class=\"col-lg-3 control-label\" for=\"".$row["column_name"]."\">".label($row["column_name"])." <?php echo form_error('".$row["column_name"]."') ?></label>
            <div class=\"col-lg-5\">  <textarea class=\"form-control\" rows=\"3\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\"><?php echo $".$row["column_name"]."; ?></textarea> </div>
        </div>";
    } else
    {
    $string .= "\n\t    <div class=\"form-group\">
            <label class=\"col-lg-3 control-label\" for=\"".$row["data_type"]."\">".label($row["column_name"])." <?php echo form_error('".$row["column_name"]."') ?></label>
           <div class=\"col-lg-5\"> <input type=\"text\" class=\"form-control\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\" value=\"<?php echo $".$row["column_name"]."; ?>\" /></div>
        </div>";
    }
}
$string .= "\n\t    <input type=\"hidden\" name=\"".$pk."\" value=\"<?php echo $".$pk."; ?>\" /> ";
$string .= "\n\t    
                                    <div class=\"form-group\">
                                        <div class=\"col-lg-9 col-lg-offset-3\">
                                            <button type=\"submit\" class=\"btn btn-primary\"><?php echo \$button ?></button>
                                       

 ";
$string .= "\n\t    
                                    
                                            <a href=\"<?php echo site_url('".$c_url."') ?>\" class=\"btn btn-default\">Cancel</a>
                                        </div>
                                    </div>
";
$string .= "\n\t
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>

   ";

$hasil_view_form = createFile($string, $target."views/" . $c_url . "/" . $v_form_file);

?>